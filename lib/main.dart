import 'package:flutter/material.dart';
import 'screens/home.dart';
import 'screens/orders.dart';
import 'screens/chat.dart';
import 'screens/inbox.dart';
import 'screens/account.dart';

void main() => runApp(GojekClone());
int i = 0;
final List<Widget> pages = [Home(), Orders(), Chat(), Inbox(), Account()];

class GojekClone extends StatelessWidget {
  const GojekClone({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Gojek",
      theme: ThemeData(
        // fontFamily: 'MaisonNeue',
        primarySwatch: Colors.green,
      ),
      home: Navigation(title: i.toString()),
    );
  }
}

class Navigation extends StatefulWidget {
  Navigation({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  void _incrementTab(index){
    setState(() {
      i = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Container(
           height: 25,
           child: Image.asset("assets/img/logo-gojek.png"),
         ),
          actions: <Widget>[
             Image.asset("assets/img/appbar-image.png", scale: 3.5,),
          ],
          elevation: 1.0,
          backgroundColor: Colors.white,
          centerTitle: false,
       ),
       body: pages[i],
       backgroundColor: Colors.white,
       bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: i,
          onTap: (index) {
            _incrementTab(index);
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text("Home")
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.receipt),
              title: Text("Orders")
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message),
              title: Text("chat")
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.mail),
              title: Text("inbox")
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text("Account")
            ),
          ],
        ),
    );
  }
}