import 'package:flutter/material.dart';

class Home extends StatelessWidget {
    // const Home({Key key}) : super(key: key);
  final List<String> menuTitle = [ "Pay", "Promo", "Topup", "More"];
  final List<String> menuImage = ["icon-pay.png", "icon-promo.png", "icon-topup.png", "icon-more.png"];
  final List<String> title = ["GoRide", "GoCar", "GoFood", "GoBlueBird", "GoSend", "GoDeals", "GoPulsa", "MORE"];
  final List<String> images = ["icon-goride.png", "icon-gocar.png", "icon-gofood.png", "icon-gobluebird.png", "icon-gosend.png", "icon-godeals.png", "icon-gopulsa.png", "icon-more2.png"];
  
  @override
  Widget build(BuildContext context) {
  print(images.length);
    return Padding(
      padding: EdgeInsets.only(right: 15.0, left: 15.0, top: 0, bottom: 0),
      child: ListView(
        children: <Widget>[
          SizedBox(height: 15.0,),
          //Top Menu Bar, Warna Biru
          Container(
            height: 100.0,
            // color: Color.fromRGBO(36, 80, 167, 1),
            decoration: BoxDecoration(
              color: Color.fromRGBO(46, 101, 187, 1),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 10,),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Image.asset("assets/img/logo-gopay.png", scale: 12,),
                      Text("Rp.9.790.000", style: TextStyle(color: Color.fromRGBO(244, 254, 255, 1), fontWeight: FontWeight.w800, fontSize: 14))
                    ],
                  ),
                ),  
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, bottom: 0.0, right: 25.0, left: 25.0),
                  child: TopMenuIcon(title: menuTitle, images: menuImage,)
                ),
              ],
            ),
          ),

          //Menu Tengah
          Container(
            height: 200.0,
            child: Padding(
              padding: EdgeInsets.only(right: 15.0, left: 20.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  MiddleMenuIcon(image: images, title: title,),
                  // MiddleMenuIcon(image: images2, title: title2)
                ],
              ),
            ),
          ),
          SizedBox(height: 10,),
          //Berita
          Container(
            height: 350.0,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 250,
                      child: Container(
                        height: 50,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text("Movies Playing Now", style: TextStyle(fontWeight: FontWeight.w800)),
                            Text("From expensive suuperhero flicks to smaller drama, here is our bet picks!", style: TextStyle(fontSize: 12), )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 50,
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: (){
                          print("hai");
                        },
                        child: Text("See All", style: TextStyle(color: Colors.green),),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10,),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Container(
                              height: 250,
                              width: 150,
                              child: Image.asset("assets/img/ferrari.jpg" ,fit: BoxFit.cover),
                            ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Container(
                              height: 250,
                              width: 150,
                              child: Image.asset("assets/img/ferrari.jpg" ,fit: BoxFit.cover),
                            ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Container(
                              height: 250,
                              width: 150,
                              child: Image.asset("assets/img/ferrari.jpg" ,fit: BoxFit.cover),
                            ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 250.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0)
                  ),
                  child: Image.asset("assets/img/banner-news.png", fit: BoxFit.fill,
                  )
                ),
                Text("Be worry-free on Pay Day!", style: TextStyle(fontWeight: FontWeight.bold),),
                Text("Because GoPay Pay Day is back with up to 60% CASHBACK in hundreds of participating merchants!", style: TextStyle(fontSize: 12),),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 30,
                      width: 75,
                      child: FlatButton(
                        color: Colors.green,
                        onPressed: (){
                          print("hai");
                        },
                        child: Text("I'M IN!", style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class TopMenuIcon extends StatelessWidget {
  const TopMenuIcon({Key key, this.title, this.images}) : super(key: key);
  final List<String> title;
  final List<String> images;
  @override
  Widget build(BuildContext context) {
    List <Widget> menuList = new List();
    for(int i = 0; i < images.length; i++){
      Column menuColumn = 
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Image.asset("assets/img/${images[i]}", scale: 10),
          SizedBox(height: 7.0),
          Text(title[i], style: TextStyle(fontSize: 11, color: Color.fromRGBO(204, 233, 253, 1), fontWeight: FontWeight.w900),)
        ],
      );
      menuList.add(menuColumn);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: menuList
    );
  }
}

class MiddleMenuIcon extends StatelessWidget {
  const MiddleMenuIcon({Key key, this.title, this.image}) : super(key: key);
  final List<String> image;
  final List<String> title;
  @override
  Widget build(BuildContext context) {
    List<Widget> menuList = new List();
    for(int i = 0; i < image.length / 2 ; i++ ){
      Column menuColumn = 
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom:12.0),
                child: Image.asset("assets/img/${image[i]}", scale: 8,)
              ),
              Text(title[i], style: TextStyle(fontSize: 11),)
            ],
          ),
          SizedBox(height: 25,),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom:12.0),
                child: Image.asset("assets/img/${image[i+4]}", scale: 8,)
              ),
              Text(title[i+4], style: TextStyle(fontSize: 11),)
            ],
          ),
        ],
      );
      
      menuList.add(menuColumn);                 
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: menuList,
    );
  }
}